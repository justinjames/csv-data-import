const csvReadParse = require('./csv-read-parse');
const csvHandlerOne = require('./csv-handler');
const csvHandlerTwo = require('./csv-handler-two');

// file path is relative to root app
csvReadParse.loadAndParse('./sample.csv', csvHandlerOne.handleCsvRow);

setTimeout(() => {
	console.log('-----------------');
	csvReadParse.loadAndParse(
		'./folder/sample2.csv',
		csvHandlerTwo.handleCsvRowTwo
	);
}, 1500);

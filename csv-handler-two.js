const myUsers = {};

module.exports.handleCsvRowTwo = row => {
	console.log('adding', row, 'to myUsers in handler 2');

	if (!myUsers[row.ASAP]) {
		myUsers[row.ASAP] = { ...row };
	}

	console.log('updated version of myUsers in handler 2', myUsers);
};

const myUsers = {};

module.exports.handleCsvRow = row => {
	console.log('adding', row, 'to myUsers in handler 1');

	if (!myUsers[row.ASAP]) {
		myUsers[row.ASAP] = { ...row };
	}

	console.log('updated version of myUsers in handler 1', myUsers);

	/*
        // Could do something here like ...

        const { ASAP, Name, Org } = row
        const query = {
            ASAP
        }
        const set = { $set: row }
        const options = { upsert: true }

        UserModel.findOneAndUpdate(query, set, options).exec((err, doc) {

        })

    */
};

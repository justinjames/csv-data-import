const fs = require('fs');
const csv = require('fast-csv');

module.exports.loadAndParse = (file, handler) => {
	fs.createReadStream(file)
		.pipe(csv.parse({ headers: true }))
		.on('data', row => {
			handler(row);
		});
};
